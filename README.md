
# KA3005P Control Software

Control your KA3005P lab power supply via USB / Serial Port

- Set any parameters remotely from your PC.
- See a live plot of current and voltage.
- Import .csv files to let the power supply automatically follow a voltage/current profile.

![screenshot](https://git.rwth-aachen.de/zaubentrucker/ka3005p-godot/-/raw/master/screenshot.png) 

## Installation

You can download an installation folder from the [release page](https://git.rwth-aachen.de/zaubentrucker/ka3005p-godot/-/releases). There are currently neither linux packages, nor windows installers available.

## Running

Just run the executable in the same folder as  the library file (.dll/.so). If you don't have a ka3005p power supply available, you can connect to a dummy device by pressing Ctrl+Shift+D in the device selection dialog.
