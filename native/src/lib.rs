mod device_manager;
mod file_picker;
mod graph;
mod program;
mod queue;
mod recorder;

use gdnative::prelude::*;

fn init(handle: InitHandle) {
    handle.add_class::<device_manager::DeviceManager>();
    handle.add_class::<file_picker::FilePicker>();
    handle.add_class::<recorder::Recorder>();
    handle.add_class::<program::Programmer>();
    handle.add_class::<graph::DynamicLine2D>();
}

godot_init!(init);
