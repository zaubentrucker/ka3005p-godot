use gdnative::prelude::*;
use nfd2::Response;
use std::sync::mpsc;
use std::thread;

const REASON_RECORDING: u64 = 0;
const REASON_PROGRAM: u64 = 1;

#[derive(NativeClass)]
#[inherit(Reference)]
#[register_with(Self::register_signals)]
pub struct FilePicker {
    #[property]
    picked_file: Option<String>,
    from_thread: Option<mpsc::Receiver<Option<String>>>,
    reason: Option<u64>,
}

#[methods]
impl FilePicker {
    fn new(_owner: &Reference) -> Self {
        FilePicker {
            picked_file: None,
            from_thread: None,
            reason: None,
        }
    }

    fn register_signals(builder: &ClassBuilder<Self>) {
        builder.signal("recording_file_picked").done();
        builder.signal("program_file_picked").done();
        builder.signal("no_program_file_picked").done();
        builder.signal("no_recording_file_picked").done();
    }

    #[export]
    fn is_picking(&self, _owner: &Reference) -> bool {
        self.from_thread.is_some()
    }

    #[export]
    fn popup_with(&mut self, _owner: &Reference, reason: u64, save: bool) {
        self.reason = Some(reason);
        let (thread_in, thread_out) = mpsc::channel();
        self.from_thread = Some(thread_out);

        thread::spawn(move || {
            let default_path = dirs::home_dir();
            let default_path = default_path.as_deref();
            let dialog_fn = if save {
                nfd2::open_save_dialog
            } else {
                nfd2::open_file_dialog
            };
            let file = match dialog_fn(None, default_path).expect("File picker error!") {
                Response::Okay(file_path) => Some(file_path.to_str().unwrap().to_string()),
                Response::OkayMultiple(_) => panic!("Expected single file!"),
                Response::Cancel => None,
            };

            thread_in
                .send(file)
                .expect("Godot thread closed before file dialog!");
        });
    }

    #[export]
    fn update(&mut self, owner: &Reference) {
        if let Some(from_thread) = self.from_thread.as_ref() {
            match from_thread.try_recv() {
                Err(mpsc::TryRecvError::Disconnected) => {
                    panic!("File picker thread not connected!")
                }
                Err(mpsc::TryRecvError::Empty) => {}
                Ok(result) => {
                    self.picked_file = result;
                    let signal = if self.picked_file.is_none() {
                        match self.reason.unwrap() {
                            REASON_PROGRAM => "no_program_file_picked",
                            REASON_RECORDING => "no_recording_file_picked",
                            _ => panic!("Got file for invalid reason!"),
                        }
                    } else {
                        // should have a reason at this point as thread must be started with one
                        match self.reason.unwrap() {
                            REASON_PROGRAM => "program_file_picked",
                            REASON_RECORDING => "recording_file_picked",
                            _ => panic!("Got file for invalid reason!"),
                        }
                    };
                    self.from_thread = None;
                    self.reason = None;
                    owner.emit_signal(signal, &[]);
                }
            }
        } else {
            panic!("There is no request pending!");
        }
    }
}
