mod program_data;

use gdnative::prelude::*;
use std::fs::File;
use std::sync::mpsc::Receiver;
use std::time::{Duration, SystemTime};

use program_data::{checker_thread, CheckFileMessage, ProgramPoint, UserData};

#[derive(NativeClass)]
#[inherit(Node)]
#[register_with(Self::register_signals)]
pub struct Programmer {
    state: ProgrammerState,
    previous_value: Option<(f64, f64)>,
    next_point: Option<ProgramPoint>,
    data_iter: Option<UserData>,
    file_path: Option<String>,
    rx_checker: Option<Receiver<CheckFileMessage>>,
}

#[derive(Clone, Copy, Debug)]
enum ProgrammerState {
    NotInitialized,
    CheckingFile,
    Running {
        start_time: SystemTime,
    },
    Stopped,
    Paused {
        start_time: SystemTime,
        paused_time: SystemTime,
    },
}

#[methods]
impl Programmer {
    fn new(_owner: &Node) -> Self {
        Programmer {
            state: ProgrammerState::NotInitialized,
            previous_value: None,
            next_point: None,
            data_iter: None,
            file_path: None,
            rx_checker: None,
        }
    }

    fn register_signals(builder: &ClassBuilder<Self>) {
        builder
            .signal("could_not_open")
            .with_param_default("error", Variant::new(""))
            .done();
        builder
            .signal("read_error")
            .with_param_default("error", Variant::new(""))
            .done();
        builder
            .signal("update_needed")
            .with_param_default("voltage", Variant::new(-1.0))
            .with_param_default("current", Variant::new(-1.0))
            .done();
        builder.signal("program_finished").done();
        builder.signal("check_complete").done();
        builder
            .signal("check_percent_update")
            .with_param_default("percent", Variant::new(-1.0))
            .with_param_default("time", Variant::new(1337))
            .with_param_default("voltage", Variant::new(-1.0))
            .with_param_default("current", Variant::new(-1.0))
            .done();
        builder
            .signal("check_error")
            .with_param_default("line", Variant::new(1337))
            .with_param_default("error_message", Variant::new(""))
            .done();
    }

    #[export]
    fn _process(&mut self, owner: &Node, _delta: f64) {
        match self.state {
            ProgrammerState::Running { start_time } => self.handle_running(owner, start_time),
            ProgrammerState::CheckingFile => self.handle_checking(owner),
            _ => {}
        }
    }

    fn handle_checking(&mut self, owner: &Node) {
        loop {
            let msg = self
                .rx_checker
                .as_mut()
                .expect("In checking-file-state but rx_checker is uninitialized!")
                .try_recv();
            match msg {
                Err(std::sync::mpsc::TryRecvError::Disconnected) => {
                    panic!("Check file thread hung up before finishing the check!");
                }
                Err(std::sync::mpsc::TryRecvError::Empty) => break,
                Ok(CheckFileMessage::Progress { progress, point }) => {
                    let ProgramPoint(time, voltage, current) = point;
                    owner.emit_signal(
                        "check_percent_update",
                        &[
                            Variant::new(progress * 100.0),
                            Variant::new(time.as_millis() as u64),
                            Variant::new(voltage),
                            Variant::new(current),
                        ],
                    );
                }
                Ok(CheckFileMessage::Finished { data_iter }) => {
                    self.rx_checker = None;
                    let mut data_iter = data_iter;
                    data_iter.reset();
                    self.data_iter = Some(data_iter);
                    self.state = ProgrammerState::Stopped;
                    owner.emit_signal("check_complete", &[]);
                    return;
                }
                Ok(CheckFileMessage::DataError { line, problem }) => {
                    self.rx_checker = None;
                    self.state = ProgrammerState::NotInitialized;
                    owner.emit_signal("check_error", &[Variant::new(line), Variant::new(problem)]);
                    return;
                }
            }
        }
    }

    fn handle_running(&mut self, owner: &Node, start_time: SystemTime) {
        fn is_point_elapsed(start_time: SystemTime, switch_time: Duration) -> Result<bool, ()> {
            let elapsed = start_time.elapsed();
            if elapsed.is_err() {
                godot_print!("Programmer: System clock went backwards! Aborting program!");
                return Err(());
            }
            let elapsed = elapsed.unwrap();
            Ok(elapsed >= switch_time)
        }

        let iter = self.data_iter.as_mut().unwrap();
        let mut update_value = None;
        while self.next_point.is_none() {
            match iter.next() {
                None => {
                    break;
                }
                Some(Err(e)) => {
                    godot_print!("Programmer: Error parsing data: {:?}! Aborting program!", e);
                    break;
                }
                Some(Ok(ProgramPoint(time, voltage, current))) => {
                    match is_point_elapsed(start_time, time) {
                        Err(()) => {
                            break;
                        }
                        Ok(false) => {
                            self.next_point = Some(ProgramPoint(time, voltage, current));
                        }
                        Ok(true) => {
                            // If we skip values they still need to be accounted for
                            godot_print!(
                                "WARNING: Never waited on value {:?}! Your timings might be too tight.",
                                ProgramPoint(time, voltage, current)
                            );
                            update_value = Some((voltage, current));
                        }
                    }
                }
            }
        }

        let should_update = match update_value {
            None => false,
            Some(new_value) => match self.previous_value {
                None => true,
                Some(old_value) => old_value != new_value,
            },
        };
        if should_update {
            let (voltage, current) = update_value.unwrap();
            owner.emit_signal(
                "update_needed",
                &[Variant::new(voltage), Variant::new(current)],
            );
        }

        if self.next_point.is_none() {
            self.state = ProgrammerState::Stopped;
            self.data_iter.as_mut().unwrap().reset();
            owner.emit_signal("program_finished", &[]);
            return;
        }

        let ProgramPoint(time, voltage, current) = self.next_point.unwrap();
        match is_point_elapsed(start_time, time) {
            Err(()) => {
                godot_print!("Programmer: System clock went backwards! Aborting program!")
            }
            Ok(true) => {
                self.next_point = None;
                owner.emit_signal(
                    "update_needed",
                    &[Variant::new(voltage), Variant::new(current)],
                );
            }
            Ok(false) => {}
        }
    }

    #[export]
    fn get_state(&mut self, _owner: &Node) -> String {
        format!("{:?}", self.state)
    }

    #[export]
    fn set_file(&mut self, owner: &Node, path: String, check_file: bool) {
        match File::open(path.clone()) {
            Err(e) => {
                owner.emit_signal("could_not_open", &[Variant::new(e.to_string())]);
            }
            Ok(file) => {
                self.next_point = None;
                let data_iter = UserData::new(file);
                self.file_path = Some(path);
                if check_file {
                    let (tx, rx) = std::sync::mpsc::channel();
                    self.rx_checker = Some(rx);
                    std::thread::spawn(move || {
                        checker_thread(data_iter, tx);
                    });
                    self.state = ProgrammerState::CheckingFile;
                } else {
                    self.data_iter = Some(data_iter);
                    self.state = ProgrammerState::Stopped;
                }
            }
        }
    }

    #[export]
    fn start(&mut self, _owner: &Node) {
        match self.state {
            ProgrammerState::NotInitialized => {
                godot_print!("Programmer: Can't start while not initialized!");
            }
            ProgrammerState::Stopped => {
                self.state = ProgrammerState::Running {
                    start_time: SystemTime::now(),
                };
            }
            ProgrammerState::Paused {
                start_time,
                paused_time,
            } => {
                let elapsed = paused_time.elapsed();
                if elapsed.is_err() {
                    godot_print!("Programmer: System time went backwards! Aborting!");
                }
                self.state = ProgrammerState::Running {
                    start_time: start_time + elapsed.unwrap(),
                };
            }
            _ => {}
        }
    }

    #[export]
    fn pause(&mut self, _owner: &Node) {
        match self.state {
            ProgrammerState::CheckingFile { .. } => {
                godot_print!("Programmer: Can't pause while checking file!");
            }
            ProgrammerState::Stopped => {
                godot_print!("Programmer: Can't pause while stopped!");
            }
            ProgrammerState::NotInitialized => {
                godot_print!("Programmer: Can't pause while not initialized!");
            }
            ProgrammerState::Paused { .. } => {
                godot_print!("Programmer: Can't pause while already paused!");
            }
            ProgrammerState::Running { start_time } => {
                self.state = ProgrammerState::Paused {
                    start_time,
                    paused_time: SystemTime::now(),
                };
            }
        }
    }
    #[export]
    fn stop(&mut self, _owner: &Node) {
        match self.state {
            ProgrammerState::Stopped => {}
            ProgrammerState::NotInitialized => {
                godot_print!("Programmer: Can't stop while not initialized!");
            }
            ProgrammerState::CheckingFile { .. } => {
                godot_print!("Programmer: Can't stop while checking file!");
            }
            ProgrammerState::Paused { .. } | ProgrammerState::Running { .. } => {
                self.data_iter
                    .as_mut()
                    .expect("Programmer: Exiting Running/Paused-State while data_iter is None!")
                    .reset();
                self.next_point = None;
                self.state = ProgrammerState::Stopped;
            }
        }
    }
}
