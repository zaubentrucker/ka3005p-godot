use ka3005p::{MAX_CURRENT_AMPERE, MAX_VOLTAGE_VOLT};
use std::fs::File;
use std::io::BufReader;
use std::io::{BufRead, Seek};
use std::sync::mpsc::Sender;
use std::time::Duration;

lazy_static::lazy_static! {
    static ref PARSE_REGEX: regex::Regex = {
        regex::Regex::new(r"^\s*(\d+)\s*,\s*(\d+\.?\d*)\s*,\s*(\d+\.?\d*)\s*$")
            .unwrap()
    };
}

/// After what share of bytes checked do we give a "check percent update"
const CHECK_UPDATE_GRANULARITY: f64 = 0.001;

#[derive(Debug)]
pub enum ParseError {
    CaptureFailed,
    ReadLineFailed(String),
}
#[derive(Clone, Copy, Debug)]
pub struct ProgramPoint(pub Duration, pub f64, pub f64);

pub struct UserData {
    file: std::io::BufReader<File>,
    progress: u64,
    file_size: u64,
}

impl Iterator for UserData {
    type Item = Result<ProgramPoint, ParseError>;

    fn next(&mut self) -> Option<Self::Item> {
        let mut line = String::new();
        let res = self.file.read_line(&mut line);
        match res {
            Ok(0) => None,
            //ignore empty lines
            Ok(1) => {
                self.progress += 1;
                self.next()
            }

            Ok(n) => {
                self.progress += n as u64;
                Some(Self::parse_line(&line))
            }
            Err(e) => Some(Err(ParseError::ReadLineFailed(e.to_string()))),
        }
    }
}

impl UserData {
    pub fn new(file: std::fs::File) -> UserData {
        let metatdata = file
            .metadata()
            .expect("Could not read metadata of program file!");
        UserData {
            file: BufReader::new(file),
            progress: 0,
            file_size: metatdata.len(),
        }
    }

    pub fn parse_line(line: &str) -> Result<ProgramPoint, ParseError> {
        match PARSE_REGEX.captures(line) {
            None => Err(ParseError::CaptureFailed),
            Some(captures) => {
                if captures.len() != 4 {
                    return Err(ParseError::CaptureFailed);
                }
                // these unwraps should not fail since they match the regex
                let time = captures[1].parse().unwrap();
                let time = Duration::from_millis(time);
                let voltage: f64 = captures[2].parse().unwrap();
                let current: f64 = captures[3].parse().unwrap();
                Ok(ProgramPoint(time, voltage, current))
            }
        }
    }

    pub fn reset(&mut self) {
        // this seek can't fail because we're only reading => no flushes needed
        self.file.rewind().unwrap();
    }

    /// Get the progress relative to the total file size.
    /// This returns a float within [0.0, 1.0]
    pub fn get_progress(&self) -> f64 {
        (self.progress as f64) / (self.file_size as f64)
    }
}

pub enum CheckFileMessage {
    Progress { progress: f64, point: ProgramPoint }, // relative progress within [0.0, 1.0]
    DataError { line: u32, problem: String },
    Finished { data_iter: UserData },
}

pub fn checker_thread(mut data_iter: UserData, tx_checker: Sender<CheckFileMessage>) {
    let mut earlier = data_iter.next();
    let mut line = 1;
    let mut previous_progress = None;
    while earlier.is_some() {
        match earlier.unwrap() {
            Err(ParseError::CaptureFailed) => {
                tx_checker
                    .send(CheckFileMessage::DataError {
                        line,
                        problem: "Couldn't parse line!".to_string(),
                    })
                    .expect("Check file thread: Main app hung up on channel!");
                return;
            }
            Err(ParseError::ReadLineFailed(e)) => {
                let problem = format!("Couldn't read next line from file!: {}", e);
                tx_checker
                    .send(CheckFileMessage::DataError { line, problem })
                    .expect("Check file thread: Main app hung up on channel!");
                return;
            }
            Ok(ProgramPoint(earlier_time, voltage, current)) => {
                let current_progress = data_iter.get_progress();
                let should_notify = if let Some(previous_progress) = previous_progress {
                    //println!("current_progress: {}\nprevious_progress: {}", current_progress, previous_progress);
                    current_progress - previous_progress > CHECK_UPDATE_GRANULARITY
                } else {
                    true // Always report the first value
                };
                if should_notify {
                    previous_progress = Some(current_progress);
                    tx_checker
                        .send(CheckFileMessage::Progress {
                            progress: data_iter.get_progress(),
                            point: ProgramPoint(earlier_time, voltage, current),
                        })
                        .expect("Check file thread: Main app hung up on channel!");
                }
                let current_point = data_iter.next();
                if let Some(Ok(ProgramPoint(current_time, voltage, current))) = current_point {
                    if current_time < earlier_time {
                        tx_checker
                            .send(CheckFileMessage::DataError {
                                line,
                                problem: "The previous line is switching after the current line!"
                                    .to_string(),
                            })
                            .expect("Check file thread: Main app hung up on channel!");
                        return;
                    }
                    if voltage > MAX_VOLTAGE_VOLT
                        || voltage < 0.0
                        || current > MAX_CURRENT_AMPERE
                        || current < 0.0
                    {
                        tx_checker
                            .send(CheckFileMessage::DataError {
                                line,
                                problem: "Voltage/Current value out of bounds!".to_string(),
                            })
                            .expect("Check file thread: Main app hung up on channel!");
                        return;
                    }
                }
                earlier = current_point;
                line += 1;
            }
        }
    }

    data_iter.reset();

    tx_checker
        .send(CheckFileMessage::Finished { data_iter })
        .expect("Check file thread: Main app hung up on channel!");
}
