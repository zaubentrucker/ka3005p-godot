use binary_search::{binary_search, Direction};
use gdnative::api::Line2D;
use gdnative::prelude::*;

const MAX_POINT_PER_SEGMENT: u32 = 100;

/// An aggregation of `Line2D`s as workaround for the buffer size
/// restriction of `Line2D` itself. Dynamically allocates new Line2D as
/// soon as the previous tip reaches `MAX_POINT_PER_SEGMENT`.
#[derive(NativeClass)]
#[inherit(Node2D)]
#[register_with(Self::register)]
pub struct DynamicLine2D {
    current_segment_length: u32,
    segments: Vec<(Ref<Line2D>, BoundingBox)>,
    style_callback: Option<(String, Ref<Object>)>,
}

#[derive(Clone, Copy)]
struct BoundingBox {
    top_left: Vector2,
    bottom_right: Vector2,
}

#[methods]
impl DynamicLine2D {
    fn new(owner: &Node2D) -> Self {
        let line2d = Line2D::new().into_shared();
        owner.add_child(line2d, false);
        DynamicLine2D {
            current_segment_length: 0,
            segments: Vec::new(),
            style_callback: None,
        }
    }

    fn register(builder: &ClassBuilder<Self>) {
        builder
            .property("segment_count")
            .with_getter(|x, _| x.segments.len() as u32)
            .done();
    }

    /// Register a callback for setting properties on newly created line segments.
    /// The callback must have the signature:
    ///   func method_name(line: Line2D) -> None
    #[export]
    fn add_style_callback(&mut self, _owner: &Node2D, method_name: String, callee: Ref<Object>) {
        self.style_callback = Some((method_name, callee));
    }

    #[export]
    fn scale(&mut self, _owner: &Node2D, factor: Vector2) {
        for line in &mut self.segments {
            let line = unsafe { line.0.assume_safe() };
            for i in 0..line.get_point_count() {
                let old_pos = line.get_point_position(i);
                let new_pos = Vector2::new(old_pos.x * factor.x, old_pos.y * factor.y);
                line.set_point_position(i, new_pos);
            }
        }
    }

    #[export]
    fn segment_bounds(&mut self, _owner: &Node2D, index: u32) -> Rect2 {
        let bounds = self
            .segments
            .get(index as usize)
            .expect("segment_bounds: Array index out of bounds!")
            .1;
        Rect2 {
            position: bounds.top_left,
            size: Vector2 {
                x: bounds.bottom_right.x - bounds.top_left.x,
                y: bounds.bottom_right.y - bounds.top_left.y,
            },
        }
    }

    #[export]
    fn point_count(&mut self, _owner: &Node2D) -> u32 {
        if let Some(last_segment) = self.segments.last() {
            let last_segment = unsafe { last_segment.0.assume_safe() };
            last_segment.get_point_count() as u32
                + (self.segments.len() - 1) as u32 * MAX_POINT_PER_SEGMENT
        } else {
            0
        }
    }

    #[export]
    fn remove_segment(&mut self, _owner: &Node2D, index: u32) {
        let segment = self.segments.remove(index as usize).0;
        unsafe { segment.assume_safe().queue_free() }
    }

    #[export]
    fn add_point(&mut self, owner: &Node2D, point: Vector2) {
        fn union(first: BoundingBox, second: BoundingBox) -> BoundingBox {
            BoundingBox {
                top_left: Vector2 {
                    x: first.top_left.x.min(second.top_left.x),
                    y: first.top_left.y.min(second.top_left.y),
                },
                bottom_right: Vector2 {
                    x: first.bottom_right.x.max(second.bottom_right.x),
                    y: first.bottom_right.y.max(second.bottom_right.y),
                },
            }
        }

        let point_bounds = BoundingBox {
            top_left: point,
            bottom_right: point,
        };

        let new_segment_required =
                                                            // Vector should be empty on first call
            self.current_segment_length >= MAX_POINT_PER_SEGMENT || self.segments.is_empty();
        if new_segment_required {
            // to keep the line unbroken, the old segment has to connect to the new one.
            if let Some(old_segment) = self.segments.last() {
                let old_segment = unsafe { old_segment.0.assume_safe() };
                old_segment.add_point(point, -1)
            }
            let new_segment = Line2D::new().into_shared();
            if let Some((method_name, callee)) = &self.style_callback {
                unsafe {
                    callee
                        .assume_safe()
                        .call(method_name, &[new_segment.to_variant()]);
                }
            }
            owner.add_child(new_segment, false);
            self.segments.push((new_segment, point_bounds));
            self.current_segment_length = 0;
        }

        let (current_segment, current_bounds) = self.segments.last_mut().unwrap();
        let current_segment = unsafe { current_segment.assume_safe() };
        current_segment.add_point(point, -1); // -1 is end of the array
        *current_bounds = union(*current_bounds, point_bounds);

        self.current_segment_length += 1;
    }

    #[export]
    fn get_point_position(&mut self, _owner: &Node2D, index: u32) -> Vector2 {
        let segment_index = index / MAX_POINT_PER_SEGMENT;
        let point_index = index % MAX_POINT_PER_SEGMENT;
        let segment = self.segments[segment_index as usize].0;
        unsafe { segment.assume_safe().get_point_position(point_index.into()) }
    }

    #[export]
    fn get_closest_point_index(&mut self, owner: &Node2D, given_x: f32) -> u32 {
        let (largest_low, _) = binary_search((0, ()), (self.point_count(owner), ()), |i| {
            if self.get_point_position(owner, i).x < given_x {
                Direction::Low(())
            } else {
                Direction::High(())
            }
        });
        largest_low.0
    }
}
