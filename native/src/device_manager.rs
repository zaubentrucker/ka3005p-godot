use crate::queue::{queue_thread, DeviceReply, DeviceRequest};
use gdnative::prelude::*;
use ka3005p::DeviceError;
use ka3005p::PowerSupply;
use std::sync::mpsc;
use std::sync::mpsc::TryRecvError;

#[derive(NativeClass)]
#[inherit(Reference)]
#[register_with(Self::register_signals)]
pub struct DeviceManager {
    error: Option<String>,
    to_device: Option<mpsc::Sender<DeviceRequest>>,
    from_device: Option<mpsc::Receiver<DeviceReply>>,

    #[property]
    actual_voltage: Option<f64>,
    #[property]
    actual_current: Option<f64>,
    #[property]
    set_voltage: Option<f64>,
    #[property]
    set_current: Option<f64>,
    #[property]
    output_state: Option<bool>,
    #[property]
    ovp: Option<bool>,
    #[property]
    ocp: Option<bool>,
    #[property]
    control_voltage: Option<bool>,
}

#[methods]
impl DeviceManager {
    fn register_signals(builder: &ClassBuilder<Self>) {
        builder.signal("set_values_changed").done();
        builder.signal("actual_values_changed").done();
        builder.signal("status_changed").done();
        builder.signal("connection_lost").done();
        builder.signal("timed_out").done();
    }

    fn new(_owner: &Reference) -> Self {
        DeviceManager {
            error: None,
            to_device: None,
            from_device: None,
            actual_voltage: None,
            actual_current: None,
            set_voltage: None,
            set_current: None,
            output_state: None,
            ovp: None,
            ocp: None,
            control_voltage: None,
        }
    }

    /// Handle pending replies from the power supply.
    #[export]
    fn update(&mut self, owner: &Reference) {
        loop {
            if self.from_device.is_none() {
                self.handle_error("Device not connected!".to_string());
                return;
            }
            let device_reply = match self.from_device.as_ref().unwrap().try_recv() {
                Err(TryRecvError::Empty) => break,
                Err(TryRecvError::Disconnected) => {
                    self.handle_error("Device thread hung up!".to_string());
                    return;
                }
                Ok(device_reply) => device_reply,
            };

            match device_reply {
                DeviceReply::Error(e) => {
                    match e {
                        DeviceError::ConnectionLost => {
                            self.to_device = None;
                            self.from_device = None;
                            owner.emit_signal("connection_lost", &[]);
                            self.handle_error("Connection to device lost!".to_string());
                        }
                        DeviceError::TimedOut => {
                            owner.emit_signal("timed_out", &[]);
                            self.handle_error("Request to serial port timed out!".to_string());
                        }
                        // TODO: set error message according to error kind
                        _ => self.handle_error("Error reported from device!".to_string()),
                    }
                }
                DeviceReply::SetValues(voltage, current) => {
                    self.set_current = Some(current);
                    self.set_voltage = Some(voltage);
                    owner.emit_signal("set_values_changed", &[]);
                }
                DeviceReply::ActualValues(voltage, current) => {
                    self.actual_voltage = Some(voltage);
                    self.actual_current = Some(current);
                    owner.emit_signal("actual_values_changed", &[]);
                }
                DeviceReply::Status(status) => {
                    self.output_state = Some(status.output_on);
                    self.ovp = Some(status.over_voltage_protection);
                    self.ocp = Some(status.over_current_protection);
                    self.control_voltage = status.control_voltage;
                    owner.emit_signal("status_changed", &[]);
                }
            }
        }
    }

    /// Sends a request to the device thread.
    fn relay_request(&mut self, request: DeviceRequest) {
        let to_device = self.to_device.as_ref();
        if to_device.is_none() {
            self.handle_error("Sent request before connecting!".to_string());
            return;
        }

        match to_device.unwrap().send(request) {
            Ok(()) => {}
            Err(_) => self.handle_error(
                "Request could not be sent! The device thread must have dropped!".to_string(),
            ),
        }
    }

    #[export]
    fn is_connected(&mut self, _owner: &Reference) -> bool {
        self.to_device.is_some()
    }

    #[export]
    fn set_output(&mut self, _owner: &Reference, value: bool) {
        self.relay_request(DeviceRequest::SetOutput(value));
    }
    #[export]
    fn set_over_voltage_protection(&mut self, _owner: &Reference, value: bool) {
        self.relay_request(DeviceRequest::SetOVP(value));
    }
    #[export]
    fn set_over_current_protection(&mut self, _owner: &Reference, value: bool) {
        self.relay_request(DeviceRequest::SetOCP(value));
    }
    #[export]
    fn set_set_voltage(&mut self, _owner: &Reference, value: f64) {
        self.relay_request(DeviceRequest::SetVoltage(value));
    }
    #[export]
    fn set_set_current(&mut self, _owner: &Reference, value: f64) {
        self.relay_request(DeviceRequest::SetCurrent(value));
    }
    #[export]
    fn save_settings(&mut self, _owner: &Reference, slot: u32) {
        self.relay_request(DeviceRequest::SaveSettings(slot));
    }

    #[export]
    fn get_set_values(&mut self, _owner: &Reference) {
        self.relay_request(DeviceRequest::GetSetValues);
    }
    #[export]
    fn get_actual_values(&mut self, _owner: &Reference) {
        self.relay_request(DeviceRequest::GetActualValues);
    }
    #[export]
    fn get_status(&mut self, _owner: &Reference) {
        self.relay_request(DeviceRequest::GetStatus);
    }

    #[export]
    fn list_devices(&mut self, _owner: &Reference) -> Option<Vec<String>> {
        match ka3005p::list_serials() {
            Ok(res) => Some(res),
            Err(DeviceError::CouldNotOpen(reason)) => {
                self.handle_error(reason);
                None
            }
            Err(_) => {
                DeviceManager::panic_wrong_error();
                None
            }
        }
    }

    fn handle_error(&mut self, error_text: String) {
        godot_print!("{}", error_text);
        self.error = Some(error_text);
    }

    fn panic_wrong_error() {
        panic!("This error should have never been received here!")
    }

    /// Tries to connect this device manager with a power supply.
    ///
    /// Returns `true` if the connection to the serial port at `device_path` could be established.
    /// If the connection could not be established, the function returns `false` and an error
    /// message will be stored in `self` that can be retrieved by calling `check_error()`.
    #[export]
    fn connect_device(&mut self, _owner: &Reference, device_path: String) -> bool {
        let device = PowerSupply::connect(&device_path, ka3005p::SERIAL_TIMEOUT);
        match device {
            Err(DeviceError::CouldNotOpen(reason)) => {
                self.handle_error(format!(
                    "Could not open device {}: \"{}\"",
                    &device_path, reason
                ));
                false
            }
            Err(_) => {
                Self::panic_wrong_error();
                false
            }
            Ok(device) => {
                let (queue_output, queue_input) = queue_thread(device);
                self.from_device = Some(queue_output);
                self.to_device = Some(queue_input);
                true
            }
        }
    }

    #[export]
    fn connect_dummy(&mut self, _owner: &Reference) {
        let device = PowerSupply::connect_dummy();
        let (queue_output, queue_input) = queue_thread(device);
        self.from_device = Some(queue_output);
        self.to_device = Some(queue_input);
    }

    #[export]
    fn disconnect_device(&mut self, _owner: &Reference) {
        self.to_device = None;
        godot_print!("Disconnecting device thread.");
    }

    #[export]
    fn check_error(&mut self, _owner: &Reference) -> Option<String> {
        self.error.take()
    }
}
