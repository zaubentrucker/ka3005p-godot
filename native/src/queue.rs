use gdnative::prelude::godot_print;
use ka3005p::{DeviceError, PowerSupply, StatusReply};
use std::sync::mpsc;
use std::thread;

pub enum DeviceRequest {
    SetOutput(bool),
    SetOCP(bool),
    SetOVP(bool),
    SetVoltage(f64),
    SetCurrent(f64),
    SaveSettings(u32),
    GetSetValues,
    GetActualValues,
    GetStatus,
}

pub enum DeviceReply {
    Error(DeviceError),
    SetValues(f64, f64),    // voltage, current
    ActualValues(f64, f64), // voltage, current
    Status(StatusReply),
}

fn handle_request(device: &mut PowerSupply, request: DeviceRequest) -> Option<DeviceReply> {
    match request {
        DeviceRequest::SetOutput(value) => match device.write_output_state(value) {
            Err(e) => Some(DeviceReply::Error(e)),
            Ok(()) => None,
        },
        DeviceRequest::GetSetValues => match device.read_set_values() {
            Err(e) => Some(DeviceReply::Error(e)),
            Ok((voltage, current)) => Some(DeviceReply::SetValues(voltage, current)),
        },
        DeviceRequest::GetActualValues => match device.read_actual_values() {
            Err(e) => Some(DeviceReply::Error(e)),
            Ok((voltage, current)) => Some(DeviceReply::ActualValues(voltage, current)),
        },
        DeviceRequest::GetStatus => match device.read_status() {
            Err(e) => Some(DeviceReply::Error(e)),
            Ok(status) => Some(DeviceReply::Status(status)),
        },
        DeviceRequest::SetOVP(value) => match device.write_over_voltage_protection(value) {
            Err(e) => Some(DeviceReply::Error(e)),
            Ok(()) => None,
        },
        DeviceRequest::SetOCP(value) => match device.write_over_current_protection(value) {
            Err(e) => Some(DeviceReply::Error(e)),
            Ok(()) => None,
        },
        DeviceRequest::SetVoltage(value) => match device.write_voltage(value) {
            Err(e) => Some(DeviceReply::Error(e)),
            Ok(()) => None,
        },
        DeviceRequest::SetCurrent(value) => match device.write_current(value) {
            Err(e) => Some(DeviceReply::Error(e)),
            Ok(()) => None,
        },
        DeviceRequest::SaveSettings(slot) => match device.save_setting(slot) {
            Err(e) => Some(DeviceReply::Error(e)),
            Ok(()) => None,
        },
    }
}

pub fn queue_thread(
    mut device: PowerSupply,
) -> (mpsc::Receiver<DeviceReply>, mpsc::Sender<DeviceRequest>) {
    let (request_out, request_in) = mpsc::channel();
    let (reply_out, reply_in) = mpsc::channel();

    thread::spawn(move || {
        let mut request_queue = Vec::new();
        loop {
            // block first, then check for more messages
            if let Ok(first_request) = request_in.recv() {
                request_queue.push(first_request)
            } else {
                // gui signals to quit by hanging up the channel.
                godot_print!("Device thread shutting down.");
                break;
            }

            while let Ok(request) = request_in.try_recv() {
                request_queue.push(request)
            }

            for request in request_queue.drain(..) {
                if let Some(reply) = handle_request(&mut device, request) {
                    if reply_out.send(reply).is_err() {
                        break;
                    }
                }
            }
        }
    });

    (reply_in, request_out)
}
