use gdnative::prelude::*;
use std::io::Write;

#[derive(NativeClass)]
#[inherit(Reference)]
#[register_with(Self::register_signals)]
pub struct Recorder {
    file: Option<std::fs::File>,
}

#[methods]
impl Recorder {
    fn new(_owner: &Reference) -> Self {
        Recorder { file: None }
    }

    fn register_signals(builder: &ClassBuilder<Self>) {
        builder
            .signal("could_not_open")
            .with_param_default("error", Variant::new(""))
            .done();
        builder
            .signal("write_error")
            .with_param_default("error", Variant::new(""))
            .done();
    }

    fn serialize_point(voltage: f64, current: f64, timestamp: u64) -> Vec<u8> {
        let string = format!("{}, {}, {}\n", timestamp, voltage, current);
        string.as_bytes().into()
    }

    #[export]
    fn add_point(&mut self, owner: &Reference, voltage: f64, current: f64, timestamp: u64) {
        if let Some(file) = self.file.as_mut() {
            let line = Self::serialize_point(voltage, current, timestamp);
            if let Err(e) = file.write_all(&line) {
                owner.emit_signal("write_error", &[Variant::new(e.to_string())]);
            }
        } else {
            godot_error!("Called add_point before setting a file!");
        }
    }

    #[export]
    fn set_file(&mut self, owner: &Reference, path: String) {
        let file = std::fs::File::create(path);
        match file {
            Ok(file) => {
                self.file = Some(file);
            }
            Err(e) => {
                owner.emit_signal("could_not_open", &[Variant::new(e.to_string())]);
            }
        }
    }
}
