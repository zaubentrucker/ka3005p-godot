extends Control


enum Error { DISCONNECTED, COULD_NOT_OPEN, COULD_NOT_WRITE, CHECK_FILE_ERROR }
enum FileOrder { RECORDING = 0, PROGRAM = 1 }

var _manager = preload("res://DeviceManager.gdns").new()
var _file_picker = preload("res://FilePicker.gdns").new()
var _recorder = preload("res://Recorder.gdns").new()
var _programmer = preload("res://Programmer.gdns").new()
var _program_points = []

var _is_recording = false

# Called when the node enters the scene tree for the first time.
func _ready():
    _manager.connect("set_values_changed", self, "_on_set_values_changed", [], CONNECT_DEFERRED)
    _manager.connect("actual_values_changed", self, "_on_actual_values_changed", [], CONNECT_DEFERRED)
    _manager.connect("status_changed", self, "_on_status_changed", [], CONNECT_DEFERRED)
    _manager.connect("connection_lost", self, "_on_connection_lost", [], CONNECT_DEFERRED)
    _manager.connect("timed_out", self, "_on_timed_out", [], CONNECT_DEFERRED)
    _file_picker.connect("recording_file_picked", self, "_on_recording_file_picked", [], CONNECT_DEFERRED)
    _file_picker.connect("program_file_picked", self, "_on_program_file_picked", [], CONNECT_DEFERRED)
    _file_picker.connect("no_program_file_picked", self, "_on_no_program_file_picked", [], CONNECT_DEFERRED)
    _file_picker.connect("no_recording_file_picked", self, "_on_no_recording_file_picked", [], CONNECT_DEFERRED)
    _recorder.connect("could_not_open", self, "_on_recorder_could_not_open", [], CONNECT_DEFERRED)
    _recorder.connect("write_error", self, "_on_recorder_write_error", [], CONNECT_DEFERRED)
    _programmer.connect("update_needed", self, "_on_programmer_update_needed")
    _programmer.connect("could_not_open", self, "_on_programmer_coud_not_open")
    _programmer.connect("check_error", self, "_on_programmer_check_error")
    _programmer.connect("check_percent_update", self, "_on_programmer_check_percent_update")
    _programmer.connect("check_complete", self, "_on_programmer_check_complete")
    _programmer.connect("program_finished", self, "_on_programmer_program_finished")
    _programmer.connect("read_error", self, "_on_programmer_read_error")
    add_child(_programmer)
    get_device_from_user()
    
func _notification(what):
    if what == NOTIFICATION_PREDELETE:
        if _manager.is_connected():
            _manager.disconnect_device()

func _process(_delta):
    if _manager.is_connected():
        _manager.update()
    if _file_picker.is_picking():
        _file_picker.update()
    
func get_device_from_user():
    $SelectDeviceDialog.popup_with(_manager.list_devices())


# ------------------------------
# Signals from the DeviceManager
func _on_set_values_changed():
    $MainView.update_set_voltage(_manager.set_voltage)
    $MainView.update_set_current(_manager.set_current)

func _on_actual_values_changed():
    var time = OS.get_system_time_msecs()
    $MainView.update_actual_voltage(_manager.actual_voltage, time)
    $MainView.update_actual_current(_manager.actual_current, time)
    if _is_recording:
        _recorder.add_point(_manager.actual_voltage, _manager.actual_current, time)
    # continuously poll for actual values so they can be diplayed and recorded
    _manager.get_actual_values()

func _on_status_changed():
    $MainView.update_set_ovp(_manager.ovp)
    $MainView.update_set_ocp(_manager.ocp)
    $MainView.update_set_output(_manager.output_state)

func _on_connection_lost():
    $ErrorDialog.popup_with(Error.DISCONNECTED, "Connection to power supply lost!")

func _on_timed_out():
    _manager.disconnect_device()
    $ErrorDialog.popup_with(Error.DISCONNECTED, "Connection to power supply timed out!")

# ----------------------------
# Signals from the file picker
func _on_recording_file_picked():
    var file_path = _file_picker.picked_file;
    _recorder.set_file(file_path)
    $MainView.update_recording_file(file_path)

func _on_program_file_picked():
    var file_path = _file_picker.picked_file;
    _programmer.set_file(file_path, true)
    $MainView.clear_program_points()
    $MainView.show_program_bar()

func _on_no_recording_file_picked():
    $MainView.update_recording_file("")

func _on_no_program_file_picked():
    pass

    
# -------------------------
# Signals from the recorder
func _on_recorder_could_not_open(error):
    $MainView.update_recording_file("")
    $ErrorDialog.popup_with(Error.COULD_NOT_OPEN, "Could not open File!:\n" + error)

func _on_recorder_write_error(error):
    $ErrorDialog.popup_with(Error.COULD_NOT_OPEN, "Error writing to recording file!:\n" + error)
    _is_recording = false
    $MainView.stop_recording()

# ---------------------------
# Signals from the Programmer
func _on_programmer_update_needed(voltage: float, current: float):
    $MainView.update_set_voltage(voltage)
    $MainView.update_set_current(current)
    _manager.set_set_voltage(voltage)
    _manager.set_set_current(current)

func _on_programmer_coud_not_open(reason: String):
    $ErrorDialog.popup_with(Error.COULD_NOT_OPEN, "Could not open file!\n" + reason)

func _on_programmer_check_error(line: int, error: String):
    _program_points = []
    $CheckingFileDialog.hide()
    $MainView.disable_program_bar()
    $ErrorDialog.popup_with(Error.CHECK_FILE_ERROR, "Error on line %s:\n%s" % [line, error])

func _on_programmer_program_finished():
    $MainView.stop_program()

func _on_programmer_check_percent_update(percent: float, time: int, voltage: float, current: float):
    $CheckingFileDialog.show()
    $CheckingFileDialog.set_progress(percent)
    _program_points.push_back([time, voltage, current])

func _on_programmer_check_complete():
    $CheckingFileDialog.hide()
    $MainView.set_program_preview(_program_points)
    _program_points = []
    
func _on_programmer_read_error(error: String):
    print("read error: %s" % error)


# -----------------------
# Signals from the Timers
func _on_RefreshStateTimer_timeout():
    if _manager.is_connected():
        _manager.get_set_values()
        _manager.get_status()


# -----------------------------------
# Signals from the SelectDeviceDialog
func _on_SelectDeviceDialog_device_selected(choice):
    var success = _manager.connect_device(choice)
    if not success:
        var reason = _manager.check_error()
        $ErrorDialog.popup_with(Error.DISCONNECTED, "Could not connect to serial port:\n\n " + reason)
    else:
        # kickoff continous polling for measured data
        _manager.get_actual_values()

func _on_SelectDeviceDialog_nothing_selected():
    $ErrorDialog.popup_with(Error.DISCONNECTED, "Please connect and select a serial port!")

func _on_SelectDeviceDialog_requested_refresh():
    $SelectDeviceDialog.set_items(_manager.list_devices())

func _on_SelectDeviceDialog_dummy_selected():
    _manager.connect_dummy()
    _manager.get_actual_values()


# ---------------------------
# Signals from the ErrorDalog
func _on_ErrorDialog_closed_with(ID):
    if ID == Error.DISCONNECTED:
        _programmer.stop()
        get_device_from_user()


# -------------------------
# Signals from the MainView
func _on_MainView_select_recording_file():
    var reason = FileOrder.RECORDING
    _file_picker.popup_with(reason, true)

func _on_MainView_set_output(value):
    _manager.set_output(value)

func _on_MainView_set_current(value):
    _manager.set_set_current(value)

func _on_MainView_set_voltage(value):
    _manager.set_set_voltage(value)

func _on_MainView_set_ocp(state):
    _manager.set_over_current_protection(state)

func _on_MainView_set_ovp(state):
    _manager.set_over_voltage_protection(state)

func _on_MainView_save_memory(slot_nr):
    _manager.save_settings(slot_nr)

func _on_MainView_should_record(state):
    _is_recording = state

func _on_MainView_program_play():
    _programmer.start()
    _manager.set_output(true)
    print('started')

func _on_MainView_program_pause():
    _programmer.pause()
    print('paused')

func _on_MainView_program_stop():
    _programmer.stop()
    print('stopped')

func _on_MainView_select_program_file():
    var reason = FileOrder.PROGRAM
    _file_picker.popup_with(reason, false)

func _on_MainView_show_about():
    $AboutDialog.popup()

func _on_MainView_show_csv_help():
    $CSVFormatDialog.popup()
