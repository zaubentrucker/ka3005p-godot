extends Node2D

class_name ControlCenterer

onready var _parent = get_parent() as Control

func _process(_delta):
    var root_size = get_tree().get_root().get_size()
    var own_size = _parent.get_size()
    _parent.set_position(Vector2(root_size[0]/2 - own_size[0]/2, root_size[1]/2 - own_size[1]/2))
