extends Node2D

class_name ControlMaximizer 

onready var child = get_children()[0] as Control

func _process(_delta):
    var root_size = get_tree().get_root().get_size()
    child.set_size(root_size)
    child.set_size(root_size)
