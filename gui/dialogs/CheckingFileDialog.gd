extends HBoxContainer


func _ready():
    $Dialog.get_close_button().visible = false

func show():
    visible = true
    $Dialog.visible = true

func hide():
    visible = false
    $Dialog.visible = false

func set_progress(percent: float):
    $Dialog/HBoxContainer/ProgressBar.value = percent
