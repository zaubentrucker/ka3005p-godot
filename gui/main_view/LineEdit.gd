extends LineEdit

signal value_entered(value)

export var unit = ""

var _last_valid: String = ""

func _ready():
    var err = connect("text_entered", self, "_on_text_entered")
    if err:
        print_debug("Failed to connect!")

func update_value(new_value: float):
    # work around bug in `steppify() with locales that use commas instead of 
    # decimal points:
    var integral_part = int(new_value)
    var fractional_part = int((new_value - integral_part) * 10)
    var new_value_text = str(integral_part) + "." + str(fractional_part)
    var new_text = new_value_text + " " + unit
    if new_text != _last_valid:
        _last_valid = new_text
        text = new_text

func _on_text_entered(new_text):
    var regex = RegEx.new()
    regex.compile("^\\s*(\\d+\\.?\\d*|\\d*\\.\\d+)\\s*V?\\s*$".format([unit]))
    var result = regex.search(new_text)
    if result:
        var new_value = float(result.get_string(1))
        emit_signal("value_entered", new_value)
        update_value(new_value)
    else:
        text = _last_valid
