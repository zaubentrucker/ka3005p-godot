extends VSlider

var _muted_connections = {}

func mute_signal(signal_name: String):
    for connection in get_signal_connection_list(signal_name):
        disconnect(signal_name, connection["target"], connection["method"])
        if _muted_connections.has(signal_name):
            _muted_connections[signal_name].push_back(connection)
        else:
            _muted_connections[signal_name] = [connection]

func unmute_signal(signal_name: String):
    for connection in _muted_connections[signal_name]:
        connect(signal_name, connection["target"], connection["method"],
                connection["binds"], connection["flags"])
    _muted_connections[signal_name] = []
