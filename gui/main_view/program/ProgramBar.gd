extends HBoxContainer

signal play
signal pause
signal stop

export var max_voltage = 30.0
export var max_current = 5.0

# the final timestamp in the program
var _max_time = 0
# the time for which the programm has already been executed
var _progress_time = 0.0

var _texture_normal_play = preload("res://main_view/program/play.svg")
var _texture_pressed_play = preload("res://main_view/program/play_pressed.svg")
var _texture_normal_pause = preload("res://main_view/program/pause.svg")
var _texture_pressed_pause = preload("res://main_view/program/pause_pressed.svg")

var _is_playing = false

onready var x_space = $Control/ColorRect.rect_size[0]
onready var _voltage_line = preload("res://DynamicLine2D.gdns").new()
onready var _current_line = preload("res://DynamicLine2D.gdns").new()

func _ready():
    var y = $Control/ColorRect.rect_size[1]
    _voltage_line.add_style_callback("voltage_style_callback", self)
    _current_line.add_style_callback("current_style_callback", self)
    $Control.add_child(_voltage_line)
    $Control.add_child(_current_line)
    _voltage_line.add_point(Vector2(0, y))
    _current_line.add_point(Vector2(0, y))
    $Control/ProgressLine.add_point(Vector2(0,0))
    $Control/ProgressLine.add_point(Vector2(0,$Control.rect_size.y))
    
func _process(delta):
    if _is_playing:
        _progress_time += delta
        var available_space = $Control.rect_size.x
        if _max_time > 0:
            $Control/ProgressLine.position.x = available_space * (_progress_time * 1000) / _max_time
        

func stop():
    _is_playing = false
    _progress_time = 0.0
    $Control/ProgressLine.position.x = 0
    _update_PlayPause(false)
    
func clear_graphs():
    while _voltage_line.point_count() > 0:
        _voltage_line.remove_segment(0)
    while _current_line.point_count() > 0:
        _current_line.remove_segment(0)

    var y = $Control/ColorRect.rect_size[1]
    _voltage_line.add_point(Vector2(0, y))
    _current_line.add_point(Vector2(0, y))

# the relative values must be within [0.0, 1.0]
func set_preview(points: Array):
    _max_time = points.back()[0]
    var previous_voltage = $Control/ColorRect.rect_size[1]
    var previous_current = $Control/ColorRect.rect_size[1]
    for point in points:
        var time = float(point[0])
        var voltage = point[1]
        var current = point[2]
        var voltage_relative = voltage / max_voltage
        var current_relative = current / max_current
        var rect_size = $Control/ColorRect.rect_size
        var x = (time / _max_time) * rect_size[0]
        var voltage_y = rect_size[1] - voltage_relative * rect_size[1]
        var current_y = rect_size[1] - current_relative * rect_size[1]
        _voltage_line.add_point(Vector2(x, previous_voltage))
        _voltage_line.add_point(Vector2(x, voltage_y))
        _current_line.add_point(Vector2(x, previous_current))
        _current_line.add_point(Vector2(x, current_y))        
        previous_voltage = voltage_y
        previous_current = current_y
        
     
func voltage_style_callback(line: Line2D):
    line.width = $Control/VoltageStyleTemplate.width
    line.default_color = $Control/VoltageStyleTemplate.default_color

func current_style_callback(line: Line2D):
    line.width = $Control/CurrentStyleTemplate.width
    line.default_color = $Control/CurrentStyleTemplate.default_color


func _update_PlayPause(is_playing: bool):
    if is_playing:
        $PlayPauseButton.texture_normal = _texture_normal_pause
        $PlayPauseButton.texture_pressed = _texture_pressed_pause
    else:
        $PlayPauseButton.texture_normal = _texture_normal_play
        $PlayPauseButton.texture_pressed = _texture_pressed_play


func _on_PlayPauseButton_pressed():
    _is_playing = not _is_playing
    _update_PlayPause(_is_playing)
    if _is_playing:
        emit_signal("play")
    else:
        emit_signal("pause")

func _on_StopButton_pressed():
    stop()
    emit_signal("stop")


func _on_ColorRect_resized():
    # Scale all graph points in x:
    var factor = $Control/ColorRect.rect_size[0] / x_space
    x_space = $Control/ColorRect.rect_size[0]
    for graph in [_current_line, _voltage_line]:
        graph.scale(Vector2(factor, 1.0))

