extends VBoxContainer

signal select_recording_file
signal should_record(state)
signal select_program_file
signal program_play
signal program_pause
signal program_stop
signal show_about
signal show_csv_help

# these signals are all emitted from the emit_blocked_control()-method using
# a string parameter:
# warning-ignore:unused_signal
signal set_output(state)
# warning-ignore:unused_signal
signal set_voltage(value)
# warning-ignore:unused_signal
signal set_current(value)
# warning-ignore:unused_signal
signal set_ovp(state)
# warning-ignore:unused_signal
signal set_ocp(state)

signal save_memory(slot_nr)

export var BLOCKER_TIMEOUT = 0.2

enum ProgramMenu { LOAD_FILE=0, TOOLBAR_HIDDEN=1, }
enum HelpMenu { CSV_FORMAT=0, ABOUT=1 }

const MEMORY_SLOT_COUNT = 5
const _timeout_signals = ["set_output","set_voltage","set_current","set_ovp",
                         "set_ocp"]
var _timeouts = {}

func _ready():
    for sig in _timeout_signals:
        var timer = Timer.new()
        timer.autostart = false
        timer.one_shot = true
        timer.wait_time = BLOCKER_TIMEOUT
        add_child(timer)
        _timeouts[sig] = timer
    
    $MenuBar/DeviceMemoryMenu.get_popup().connect(
        "id_pressed", self, "_on_DeviceMemoryItem_pressed")
    $MenuBar/ProgramMenu.get_popup().connect(
        "id_pressed", self, "_on_ProgramItem_pressed")
    $MenuBar/HelpMenu.get_popup().connect(
        "id_pressed", self, "_on_HelpItem_pressed")

func emit_blocked_control(to_emit: String, value):
    (_timeouts[to_emit] as Timer).start()
    emit_signal(to_emit, value)

func is_blocked(signal_name: String) -> bool:
    return (_timeouts[signal_name] as Timer).time_left != 0

func show_program_bar():
    $ProgramBar.show()
    $MenuBar/ProgramMenu.get_popup().set_item_checked(ProgramMenu.TOOLBAR_HIDDEN, false)
    $MenuBar/ProgramMenu.get_popup().set_item_disabled(ProgramMenu.TOOLBAR_HIDDEN, false)


# -----------------------
# Updates from the Main node
func stop_recording():
    $RecordingButtons.stop()

func stop_program():
    $ProgramBar.stop()

func update_recording_file(new_path: String):
    $RecordingButtons.set_file(new_path)

func update_set_output(new_value: bool):
    if not is_blocked("set_output"):
        $HSplitContainer2/HBoxContainer/OutputButton.pressed = new_value

func update_set_voltage(new_value: float):
    if not is_blocked("set_voltage"):
        var line_edit = $HSplitContainer/GridContainer/VBoxContainer/VoltageEdit
        line_edit.update_value(new_value)
        $HSplitContainer/GridContainer/VoltageSlider.mute_signal("value_changed")
        $HSplitContainer/GridContainer/VoltageSlider.value = new_value
        $HSplitContainer/GridContainer/VoltageSlider.unmute_signal("value_changed")

func update_set_current(new_value: float):
    if not is_blocked("set_current"):
        var line_edit = $HSplitContainer/GridContainer/VBoxContainer2/CurrentEdit
        line_edit.update_value(new_value)
        $HSplitContainer/GridContainer/CurrentSlider.mute_signal("value_changed")
        $HSplitContainer/GridContainer/CurrentSlider.value = new_value
        $HSplitContainer/GridContainer/CurrentSlider.unmute_signal("value_changed")

func update_set_ovp(new_value: bool):
    if not is_blocked("set_ovp"):
        $HSplitContainer2/VBoxContainer/OVPButton.pressed = new_value

func update_set_ocp(new_value: bool):
    if not is_blocked("set_ocp"):
        $HSplitContainer2/VBoxContainer/OCPButton.pressed = new_value

func update_actual_voltage(new_value: float, epoch_timestamp: int):
    $HSplitContainer/GridContainer/VoltageGraph.add_point(epoch_timestamp, new_value)

func update_actual_current(new_value: float, epoch_timestamp: int):
    $HSplitContainer/GridContainer/CurrentGraph.add_point(epoch_timestamp, new_value)

func set_program_preview(points: Array):
    $ProgramBar.set_preview(points)

func clear_program_points():
    $ProgramBar.clear_graphs()
    
func disable_program_bar():
    $ProgramBar.hide()
    $MenuBar/ProgramMenu.get_popup().set_item_checked(ProgramMenu.TOOLBAR_HIDDEN, true)
    $MenuBar/ProgramMenu.get_popup().set_item_disabled(ProgramMenu.TOOLBAR_HIDDEN, true)

# ---------------------------
# Signals from the ProgramBar
func _on_ProgramBar_play():
    emit_signal("program_play")

func _on_ProgramBar_pause():
    emit_signal("program_pause")

func _on_ProgramBar_stop():
        emit_signal("program_stop")


# -------------------------
# Signals from the controls
func _on_ProgramItem_pressed(item_id: int):
    if item_id == ProgramMenu.LOAD_FILE:
        $ProgramBar.stop()
        emit_signal("select_program_file")
    if item_id == ProgramMenu.TOOLBAR_HIDDEN:
        $MenuBar/ProgramMenu.get_popup().set_item_checked(ProgramMenu.TOOLBAR_HIDDEN, $ProgramBar.visible)
        $ProgramBar.visible = !$ProgramBar.visible

func _on_HelpItem_pressed(item_id: int):
    if item_id == HelpMenu.ABOUT:
        emit_signal("show_about")
    if item_id == HelpMenu.CSV_FORMAT:
        emit_signal("show_csv_help")

func _on_request_recording_file():
    emit_signal("select_recording_file")

func _on_should_record(state: bool):
    emit_signal("should_record", state)

func _on_Button_toggled(button_pressed):
    emit_blocked_control("set_output", button_pressed)

func _on_VoltageSlider_value_changed(value):
    $HSplitContainer/GridContainer/VoltageGraph.set_supposed_value(value)
    var line_edit = $HSplitContainer/GridContainer/VBoxContainer/VoltageEdit
    line_edit.update_value(value)
    emit_blocked_control("set_voltage", value)

func _on_CurrentSlider_value_changed(value):
    $HSplitContainer/GridContainer/CurrentGraph.set_supposed_value(value)
    var line_edit = $HSplitContainer/GridContainer/VBoxContainer2/CurrentEdit
    line_edit.update_value(value)
    emit_blocked_control("set_current", value)

func _on_OVP_Button_toggled(button_pressed):
    emit_blocked_control("set_ovp", button_pressed)

func _on_OCP_Button_toggled(button_pressed):
    emit_blocked_control("set_ocp", button_pressed)

func _on_DeviceMemoryItem_pressed(item_id):
    emit_signal("save_memory", item_id)

func _on_VoltageEdit_value_entered(value):
    emit_signal("set_voltage", value)

func _on_CurrentEdit_value_entered(value):
    emit_signal("set_current", value)
