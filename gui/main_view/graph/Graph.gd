extends Control

export var pixel_per_second : float = 10
export var max_value : float = 30
export var min_value : float = -4
export var unit: String = ""
export var tooltip_border_width = 4
export var graph_color = Color("284c6b")

var _start_time: int

# Save the current available space to compare on resize
onready var y_space = rect_size[1]
onready var segments = preload("res://DynamicLine2D.gdns").new()

var is_mouse_hovering = false
var last_mouse_position = Vector2(0,0)

func _ready():
    $Line2DStyleTemplate.default_color = graph_color
    _start_time = OS.get_system_time_msecs()
    $CurrentValue.add_point(Vector2(0,0))
    $CurrentValue.add_point(Vector2(rect_size[0], 0))
    segments.add_style_callback("segment_style_callback", self)
    add_child(segments)
    $ToolTip.visible = false

func set_supposed_value(value: float):
    $CurrentValue.position.y = to_local_coordinates(value)
    
# Calculate the position within rect_size for a given input value
func to_local_coordinates(value: float) -> float:
    var y_offset = (value-min_value)/(max_value - min_value)
    return (1 - y_offset) * y_space

# Calculate the shown value from the given y_position within rect_size
func from_local_coordinates(y_position):
    var relative = (rect_size[1] - y_position) / rect_size[1]
    return relative * (max_value - min_value) + min_value
    
func segment_style_callback(line: Line2D):
        var already_styled = $Line2DStyleTemplate
        line.width = already_styled.width
        line.default_color = already_styled.default_color

# Add a point to be displayed by the graph
# epoch time is interpreted like OS.get_system_time_msecs()
func add_point(epoch_time: int, value: float):
    var total_passed_time = (epoch_time - _start_time) / 1000.0
    var x_position = total_passed_time * pixel_per_second
    var y_position = to_local_coordinates(value)
    
    segments.add_point(Vector2(x_position, y_position))

func _gui_input(event):
    if event is InputEventMouseMotion:
        last_mouse_position = event.position

# Find the point on the graph thats closest to the given x_value and return
# its position relative to the scene root.
func closest_graph_point(x_value: float) -> Vector2:
    # move control-space x value to segment-space:
    var moved_x_value = x_value - segments.position.x
    var index = segments.get_closest_point_index(moved_x_value)
    # go back to control-space:
    return segments.get_point_position(index) + segments.position

func _process(_delta):
    # Move segments according to passed time:
    # I avoid using delta here as that might introduce rounding errors in the
    # long run.
    var total_elapsed = (OS.get_system_time_msecs() -_start_time) / 1000.0
    var total_pixel_to_move = - total_elapsed * pixel_per_second + rect_size[0]
    segments.position.x = total_pixel_to_move
    # Delete segments that aren't on screen anymore:
    if segments.point_count() > 0:
        var oldest_bounds = segments.segment_bounds(0) as Rect2
        oldest_bounds.position += segments.position
        if not oldest_bounds.intersects(Rect2(Vector2(0,0), rect_size)):
            segments.remove_segment(0)
    
        # Set tooltip position and size:
        if is_mouse_hovering:
            var leftmost_point = closest_graph_point(0)
            var closest_point = closest_graph_point(last_mouse_position.x)
            var updated_x = 0
            # check if the closest point is the start of the graph
            if (leftmost_point - closest_point).length_squared() > 0.1:
                updated_x = last_mouse_position.x
            else:
                updated_x = closest_point.x
            var updated_y = closest_point.y
            $ToolTip.position = Vector2(updated_x, updated_y)
            var tooltip_value = stepify(from_local_coordinates(updated_y), 0.01)
            var tooltip_text = str(tooltip_value) + " " + unit
            $ToolTip/Label.text = tooltip_text
            resize_tooltip(tooltip_text)

func resize_tooltip(tooltip_text: String):
    var text_size = $ToolTip/Label.get_font("").get_string_size(tooltip_text)
    var new_polygon = [
        Vector2(0,0),
        Vector2(-20,-20),
        Vector2(-text_size.x-tooltip_border_width*2,-20),
        Vector2(-text_size.x-tooltip_border_width*2, -text_size.y-20-tooltip_border_width*2),
        Vector2(0, -text_size.y-20-tooltip_border_width*2),
       ]
    $ToolTip/Polygon2D.polygon = new_polygon
    var text_pos = Vector2(-tooltip_border_width,-20-tooltip_border_width)
    text_pos -= $ToolTip/Label.rect_size
    $ToolTip/Label.rect_position = text_pos

func _on_Graph_resized():
    # Scale all graph points in y:
    var factor = rect_size[1] / y_space
    y_space = rect_size[1]
    segments.scale(Vector2(1.0, factor))
    
    var current_value_y = $CurrentValue.get_point_position(1).y
    $CurrentValue.set_point_position(1, Vector2(rect_size[0], current_value_y*factor))


func _on_Graph_mouse_entered():
    is_mouse_hovering = true
    $ToolTip.visible = true

func _on_Graph_mouse_exited():
    is_mouse_hovering = false
    $ToolTip.visible = false
