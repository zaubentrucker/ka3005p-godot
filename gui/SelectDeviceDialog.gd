extends AcceptDialog

signal device_selected(choice)
signal dummy_selected
signal nothing_selected
signal requested_refresh(device_list)

onready var _item_list = $VBoxContainer/ItemList


# Called when the node enters the scene tree for the first time.
func _ready():
    pass # Replace with function body.
    

func popup_with(device_list):
    set_items(device_list)
    self.popup_centered()
    get_close_button().visible = false

func set_items(device_list):
    _item_list.clear()
    for port in device_list:
        _item_list.add_item(port);
    if len(device_list) > 0:
        _item_list.select(0)
    else:
        _item_list.add_item("NO DEVICE FOUND!")
        _item_list.set_item_selectable(0, false)

func _on_SelectDeviceDialog_confirmed():
    if _item_list.is_anything_selected():
        assert(len(_item_list.get_selected_items()) == 1)
        var index = _item_list.get_selected_items()[0]
        var selected_path = _item_list.get_item_text(index)
        emit_signal("device_selected", selected_path)
    else:
        emit_signal("nothing_selected")

func _on_RefreshButton_pressed():
    emit_signal("requested_refresh")

func _input(event):
    if event.is_action_pressed("connect_dummy_device"):
        emit_signal("dummy_selected")
        hide()
