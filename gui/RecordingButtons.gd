extends HBoxContainer

signal should_record(state)
signal request_recording_file

var _orig_text = "Choose Recording file..."
var _has_file = false
var _aquiring_file = false

func set_file(path):
    if path == "":
        $RecordingFileButton.text = _orig_text
        _has_file = false
        _aquiring_file = false
        $RecordDataButton.pressed = false
    else:
        $RecordingFileButton.text = path
        _has_file = true
        if _aquiring_file:
            _aquiring_file = false
            emit_signal("should_record", true)

func stop():
    $RecordDataButton.pressed = false

func _on_RecordDataButton_toggled(button_pressed):
    if button_pressed:
        if _has_file:
            emit_signal("should_record", true)
        else:
            _aquiring_file = true
            emit_signal("request_recording_file")
    else:
        emit_signal("should_record", false)

func _on_RecordingFileButton_pressed():
    emit_signal("request_recording_file")
