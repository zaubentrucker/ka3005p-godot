extends AcceptDialog

signal closed_with(ID)

var _error_id = 0

func popup_with(error_id_ , text):
    self._error_id = error_id_
    set_text(text)
    popup_centered()

func _on_ErrorDialog_popup_hide():
    emit_signal("closed_with", _error_id)
